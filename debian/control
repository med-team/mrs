Source: mrs
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Maarten L. Hekkelman <m.hekkelman@cmbi.ru.nl>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libboost-dev,
               libboost-filesystem-dev,
               libboost-system-dev,
               libboost-regex-dev,
               libboost-math-dev,
               libboost-thread-dev,
               libboost-program-options-dev,
               libboost-iostreams-dev,
               libboost-timer-dev,
               libboost-random-dev,
               libboost-chrono-dev,
               libzeep-dev,
               libperl-dev,
               liblog4cpp5-dev,
               zlib1g-dev,
               libbz2-dev,
               libjs-jquery,
               libjs-jquery-ui
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/mrs
Vcs-Git: https://salsa.debian.org/med-team/mrs.git
Homepage: http://mrs.cmbi.ru.nl/
Rules-Requires-Root: no

Package: mrs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         lsb-base,
         adduser,
         rsync,
         clustalo,
         libjs-jquery,
         libjs-jquery-ui
Description: Information Retrieval System for Biomedical databanks
 MRS is a complete system to retrieve, index and query biological and
 medical databanks. It comes with all the code required to fetch data
 using FTP or rsync, then creates a local databank with indices using
 a databank specific parser written in Perl. It can then serve this
 data using a built-in web application and webservices.
 .
 Searching can be done on words and Boolean queries. As a bonus you
 can search protein sequences using a custom Blast algorithm.
